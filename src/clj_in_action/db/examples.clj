(ns clj-in-action.db.examples
  (:require [clj-in-action.db.model.user :as user]
            [clj-in-action.db.model.charge :as charge])
  (:use [clojure.pprint :only [pprint]]))

;; Also need to look at Korma
(comment 
  (user/create {:login "gtrepanier"
                :first_name "Ginger"
                :last_name "Trepanier"
                :password "secret"
                :email_address "gtrep@foo.com"})

  (user/create {:login "atrepanier"
                :first_name "Adam"
                :last_name "Trepanier"
                :password "secret"
                :email_address "atrep@foo.com"})

  (user/create {:login "rtrepanier"
                :first_name "Rocky"
                :last_name "Trepanier"
                :password "secret"
                :email_address "rtrep@foo.com"})

  (user/create {:login "wtrepanier"
                :first_name "Wyatt"
                :last_name "Trepanier"
                :password "secret"
                :email_address "wtrep@foo.com"}) 

  (user/get-record 1)

  (pprint (user/find-records {:login "gtrepanier"}))

  (pprint (user/find-records {:first_name "Adam" :last_name "Trepanier"}))

  (pprint (user/find-records {:last_name "Trepanier"}))

  (pprint (user/update {:password "new-pass" :id 3})) 

  ;; Even if the record doesn't exist it still returns nil
  (pprint (user/update {:password "new-pass" :id 31}))

  (user/destroy-record {:id 100})

  (charge/create {:user_id 1, :amount_dollars 27 :amount_cents 91 
                  :category "meals" :vendor_name "stacks" :date "2010-01-15"})

  (pprint (charge/find-records {:category "meals"}))

  (let [adam (user/find-record {:login "gtrepanier"})]
    (pprint (user/find-charges adam)))

  (let [errors (charge/validate {:amount_dollars 0 :amount_cents -10
                                 :date "2010-01-01" :vendor_name "amazon"})]
    (println errors))

  (charge/create {:amount_cents 0 :amount_dollars 10 :vendor_name "amazon"
                  :date "2010-01-01"})
  )
