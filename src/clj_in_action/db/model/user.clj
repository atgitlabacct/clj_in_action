(ns clj-in-action.db.model.user
  (:require [clj-record.boot]))

(def db
  {:classname "com.mysql.jdbc.Driver"
   :subprotocol "mysql"
   :user "root"
   :password "root"
   :subname "//localhost/clj_in_action"})

(clj-record.core/init-model
  (:associations
    (has-many charges)))

