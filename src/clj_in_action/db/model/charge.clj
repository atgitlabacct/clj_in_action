(ns clj-in-action.db.model.charge
  (:require [clj-record.boot]))

(def db
  {:classname "com.mysql.jdbc.Driver"
   :subprotocol "mysql"
   :user "root"
   :password "root"
   :subname "//localhost/clj_in_action"})


(clj-record.core/init-model
  (associations
    (belongs-to user))
  (:validation
    (:amount_dollars "Must be positive!" #(>= % 0))
    (:amount_cents "Must be positive!" #(>= % 0)))
  (:callbacks
    (:before-save (fn [record] 
                    (if-not (:category record)
                      (assoc record :category "uncategorized")
                      record)))))
