(ns clj-in-action.multimethods.examples)

(def bob {:name "Bob the Builder" :salary 50000 :referrer :mint.com})
(def jim {:name "Jim Henson" :salary 150000 :referrer :google})
(def john {:name "John Doe" :salary 30000 :referrer :some-unknown.com})

(defn fee-amount
  [percentage user]
  (float (* 0.01 percentage (:salary user))))

(defmulti affiliate-fee :referrer)

(defmethod affiliate-fee :mint.com [user]
  (fee-amount 0.03 user))

(defmethod affiliate-fee :google.com [user]
  (fee-amount 0.01 user))

(defmethod affiliate-fee :default [user]
  (fee-amount 0.02 user))

(affiliate-fee bob) ;; Bob gets 15
(affiliate-fee jim) ;; Jim gets 30
(affiliate-fee john) ;; John gets 6
