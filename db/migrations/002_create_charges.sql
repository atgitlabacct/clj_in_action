CREATE TABLE `clj_in_action`.`charges` (
  `id` INT NOT NULL,
  `user_id` INT NULL,
  `amount_dollars` INT NULL,
  `amount_cents` INT NULL,
  `category` VARCHAR(255) NULL,
  `vendor_name` VARCHAR(255) NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`id`));
